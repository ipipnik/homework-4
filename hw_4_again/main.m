//
//  main.m
//  hw_4_again
//
//  Created by kravinov on 11/20/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
