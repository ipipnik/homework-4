//
//  MainViewController.h
//  hw_4_again
//
//  Created by kravinov on 11/20/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface MainViewController : UIViewController {
    Model *items;
}

@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *firstName;
@property (weak, nonatomic) IBOutlet UILabel *lastName;
@property (weak, nonatomic) IBOutlet UILabel *middleName;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *country;

@end
