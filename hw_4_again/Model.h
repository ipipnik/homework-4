//
//  Model.h
//  hw_4_again
//
//  Created by kravinov on 11/22/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface Model : NSObject {

}

@property (nonatomic) UIImage *photo;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;
@property (nonatomic) NSString *middleName;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *phone;
@property (nonatomic) NSString *country;

@property (nonatomic) BOOL isEmpty;

+ (Model*) singleObj;

@end
