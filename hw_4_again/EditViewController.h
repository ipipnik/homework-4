//
//  EditViewController.h
//  hw_4_again
//
//  Created by kravinov on 11/20/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface EditViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
    
    Model *items;
}

@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *middleName;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UILabel *country;

-(IBAction)takePhoto:(id)sender;

@end
