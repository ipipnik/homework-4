//
//  Model.m
//  hw_4_again
//
//  Created by kravinov on 11/22/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "Model.h"

@implementation Model {
    Model *anotherSingle;
}

+ (Model *) singleObj {
    
    static Model *single =nil;
    
    @synchronized (self) {
        if (!single) {
            single = [[Model alloc] init];
        }
    }
    return single;
}

@end
