//
//  MainViewController.m
//  hw_4_again
//
//  Created by kravinov on 11/20/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "MainViewController.h"
#import "EditViewController.h"
#import "Model.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    items = [Model singleObj];
    items.isEmpty = YES;
}


- (void)viewWillAppear:(BOOL)animated{
    // Скрыть NavigationBar
    [self.navigationController setNavigationBarHidden:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //Если не было ни одного сохранения данных и в модель грузить нечего, заполняем поля дефолтными значениями
    if ([defaults objectForKey:@"firstName"] != NULL) {
        items.isEmpty = NO;
    }
    
    if (items.isEmpty == YES) {
        _firstName.text = @"First Name";
        _lastName.text = @"Last Name";
        _middleName.text = @"Middle Name";
        _email.text = @"Email address";
        _phone.text = @"Phone number";
        _country.text = @"Country";
    }
    
    else
    {
        // Грузим картинку
        NSString *imagePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *imageName = [imagePath stringByAppendingPathComponent:@"MainImage.jpg"];
        items.photo = [UIImage imageWithContentsOfFile:imageName];
        // ---
        
        // Грузим текстовые поля
        _photo.image = items.photo;
        _firstName.text = [defaults objectForKey:@"firstName"];
        _lastName.text = [defaults objectForKey:@"lastName"];
        _middleName.text = [defaults objectForKey:@"middleName"];
        _email.text = [defaults objectForKey:@"email"];
        _phone.text = [defaults objectForKey:@"phone"];
        _country.text = [defaults objectForKey:@"country"];
        // ---
    }
}


- (IBAction)editInfo:(UIButton *)sender {
    EditViewController *editController = [[EditViewController alloc] initWithNibName:@"EditViewController" bundle:nil];
    
    [self.navigationController pushViewController:editController animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
