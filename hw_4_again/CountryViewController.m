//
//  CountryViewController.m
//  hw_4_again
//
//  Created by kravinov on 11/20/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "CountryViewController.h"

@interface CountryViewController ()

@end

@implementation CountryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"Choose country";
    
    items = [Model singleObj];
    
    countriesTable.dataSource = self;
    countriesTable.delegate = self;
    countriesSearchBar.delegate = self;
    
    countries = [[NSMutableArray alloc] init];
    NSLocale *currentLocale = [NSLocale currentLocale];
    for (NSString *code in [NSLocale ISOCountryCodes]){
        NSString *localizedCountry = [currentLocale localizedStringForCountryCode: code];
        if (localizedCountry){
            [countries addObject: localizedCountry];
        }
    };
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(isFilltered)
    {
        return [filteredCounties count];
    }
    return [countries count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    if(!isFilltered)
    {
        cell.textLabel.text = [countries objectAtIndex:indexPath.row];
    }else
    {
        cell.textLabel.text = [filteredCounties objectAtIndex:indexPath.row];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isFilltered == YES) {
        items.country = [filteredCounties objectAtIndex:indexPath.row];
    }
    else
    {
        items.country = [countries objectAtIndex:indexPath.row];
        //[NSLog(@"wokr^^");
    }
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - SearchBar Method

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        isFilltered = NO;
    }
    else
    {
        isFilltered = YES;
        filteredCounties = [[NSMutableArray alloc] init];
        
        
        for (NSString *str in countries) {
            NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(stringRange.location != NSNotFound)
            {
                [filteredCounties addObject:str];
            }
        }
    }
    [countriesTable reloadData];
}

@end
