//
//  CountryViewController.h
//  hw_4_again
//
//  Created by kravinov on 11/20/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface CountryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    
    IBOutlet UITableView *countriesTable;
    IBOutlet UISearchBar *countriesSearchBar;
    NSMutableArray *countries;
    NSMutableArray *filteredCounties;
    BOOL isFilltered;
    
    Model *items;
    
}

@end
