//
//  EditViewController.m
//  hw_4_again
//
//  Created by kravinov on 11/20/16.
//  Copyright © 2016 kravinov. All rights reserved.
//

#import "EditViewController.h"
#import "CountryViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>


@interface EditViewController ()

@property (strong, nonatomic) UIImagePickerController *imagePicker;
@property (strong, nonatomic) IBOutlet UIImageView *choosenImageView;

@end

@implementation EditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.navigationItem.title = @"Edit";
    
    // Кнопка "Done" в NavigationBar
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(saveChanges:)];
    
    items = [Model singleObj];
}


- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];

    //Обновляем значение "Страна" при переходе из CountryViewContriller
    _country.text = items.country;
    
    NSLog(@"%@", items.country);
}


- (void)saveChanges:(id)sender {
    // Регулярка для e-mail
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    // Регулярка для телефона
    NSString *phoneRegEx = @"[+]+[0-9]{11}";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegEx];

    // Проверка пустых полей
    if ((_firstName.text.length && _lastName.text.length && _middleName.text.length && _email.text.length && _phone.text.length) == 0) {
        UIAlertView *errorEmpty = [[UIAlertView alloc] initWithTitle:@"Field Check"
                                                       message:@"Some fields are empty"
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil, nil];
        [errorEmpty show];
        return;
    }
    
    // Проверка почты
    else if  ([emailTest evaluateWithObject:_email.text] != YES && [_email.text length]!=0)
    {
        UIAlertView *errorEmail = [[UIAlertView alloc] initWithTitle:@"E-mail Ckeck"
                                                             message:@"Please enter valid email address"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [errorEmail show];
        return;
    }
    
    // Проверка телефона
    else if ([phoneTest evaluateWithObject:_phone.text] !=YES)
    {
        UIAlertView *errorPhone = [[UIAlertView alloc] initWithTitle:@"Phone Ckeck"
                                                             message:@"Please enter valid phone number"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [errorPhone show];
        return;
    }
    
    // Проверка страны
    else if (_country.text.length == 0) {
        UIAlertView *errorCoutry = [[UIAlertView alloc] initWithTitle:@"Coutry Ckeck"
                                                             message:@"Please Choose The Coutry"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [errorCoutry show];
        return;
    }
    
    // Проверки пройдены, сохраняем данные
    else
    {
        items.isEmpty = NO;
        
        
        // Значение текстовых полей передаем в модель
        items.firstName = _firstName.text;
        items.lastName = _lastName.text;
        items.middleName = _middleName.text;
        items.email = _email.text;
        items.phone = _phone.text;
        items.country = _country.text;
        // ---
        
        // Сохраняем картинку
        NSString *imagePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *imageName = [imagePath stringByAppendingPathComponent:@"MainImage.jpg"];
        NSData *imageData = UIImageJPEGRepresentation(self.choosenImageView.image, 1.0);
        BOOL result = [imageData writeToFile:imageName atomically:YES];
        NSLog(@"Saved to %@? %@", imageName, (result? @"YES": @"NO"));
        // ---
        
        // Сохраняем текстовые поля
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:items.firstName forKey:@"firstName"];
        [defaults setObject:items.lastName forKey:@"lastName"];
        [defaults setObject:items.middleName forKey:@"middleName"];
        [defaults setObject:items.email forKey:@"email"];
        [defaults setObject:items.phone forKey:@"phone"];
        [defaults setObject:items.country forKey:@"country"];

        [defaults synchronize];
        // ---
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (IBAction)changeCountry:(UIButton *)sender {
    CountryViewController *countryController = [[CountryViewController alloc] initWithNibName:@"CountryViewController" bundle:nil];
    
    [self.navigationController pushViewController:countryController animated:YES];
}


// Скрыть клавиатуру по нажатию в любом пустом месте экрана приложения
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UIView* view in self.view.subviews)
        [view resignFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Photo Edit Methods

- (IBAction)takePhoto:(id)sender {
    
    _imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    _imagePicker.allowsEditing = YES;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    self.imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString*)kUTTypeImage, nil];
    [self presentViewController:self.imagePicker animated:NO completion:nil];
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary*)info {

    UIImage *chosenImage = info [UIImagePickerControllerEditedImage];
    self.choosenImageView.image = chosenImage;
    items.photo = self.choosenImageView.image;
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {

    [self dismissViewControllerAnimated:YES completion:nil];

}

@end
